# Powershell für Bash-Umsteiger

Als erfahrene Linux-Administratoren, die mit Bash oder Zsh vertraut sind, werden Sie feststellen, dass PowerShell eine einzigartige und leistungsfähige Shell-Umgebung bietet, die für die Verwaltung von Windows-Systemen optimiert ist, aber auch zunehmend auf Linux- und MacOS-Systemen eingesetzt wird. Hier sind einige wichtige Aspekte und Vorteile von PowerShell im Vergleich zu traditionellen Unix-Shells:

### Objektorientierte Natur

1. **Objektbasierte Ausgabe**: Im Gegensatz zu Bash oder Zsh, die hauptsächlich mit Text arbeiten, behandelt PowerShell alles als Objekte. Kommandos (Cmdlets) geben Objekte zurück, keine bloßen Strings, was die Datenmanipulation und -verarbeitung erleichtert.

2. **Reichhaltige Objektmanipulation**: Aufgrund seiner .NET-Basis können Sie komplexe Operationen auf Objekten ausführen, wie das Filtern, Sortieren und Transformieren von Daten in strukturierter Weise.

### Leistungsstarke Scripting-Fähigkeiten

1. **Erweiterte Scripting-Features**: PowerShell bietet fortgeschrittene Programmierkonstrukte wie Klassen, Ausnahmebehandlung, und funktionale Programmierfähigkeiten, die über das hinausgehen, was in traditionellen Shells verfügbar ist.

2. **Konsistente Syntax**: PowerShell verwendet eine konsistente Syntax für alle Cmdlets, was das Lernen und die Verwendung der Shell erleichtert. Die Verb-Nomen-Konvention (`Get-Command`, `Set-Item`, etc.) macht die Funktion eines Cmdlets sofort erkennbar.

### Umfangreiche Windows-Integration

1. **Nahtlose Windows-Verwaltung**: PowerShell ist tief in das Windows-Ökosystem integriert, was die Automatisierung von Windows-Aufgaben, wie Active Directory-Management, Windows Update und andere systemnahe Funktionen, vereinfacht.

2. **Zugriff auf .NET Framework**: PowerShell ermöglicht direkten Zugriff auf das .NET Framework, sodass Administratoren und Entwickler leistungsstarke Skripte und Anwendungen erstellen können.

### Cross-Platform-Kompatibilität

1. **PowerShell Core**: Mit der Einführung von PowerShell Core, einer Open-Source-Version von PowerShell, können Sie nun PowerShell-Skripte auf Linux- und MacOS-Systemen ausführen, was eine plattformübergreifende Verwaltung ermöglicht.

2. **SSH-Unterstützung**: PowerShell unterstützt SSH, was die Remote-Verwaltung von Linux-Systemen erleichtert und somit eine Brücke zwischen Windows- und Unix-Systemen schlägt.

### Community und Ressourcen

1. **Starke Community**: PowerShell hat eine aktive und wachsende Community, unterstützt durch Microsoft und Open-Source-Beiträge, was zu einer Fülle von Ressourcen, Modulen und Hilfswerkzeugen führt.

2. **Mächtige Tools und Integration**: Integration mit Tools wie Visual Studio Code, umfangreiche Debugging-Tools und das PowerShell Gallery-Repository für Module und Skripte erweitern die Möglichkeiten von PowerShell weit über traditionelle Shells hinaus.

## Fallstricke

Der Umstieg von Bash auf PowerShell kann eine Herausforderung sein, insbesondere für erfahrene Linux-Administratoren, die mit den Konventionen und Tools von Unix-ähnlichen Systemen vertraut sind. Hier sind einige der wichtigsten Nachteile und Fallen, auf die Sie achten sollten:

### 1. Unterschiedliche Syntax und Paradigmen

- **Syntax**: PowerShell verwendet eine andere Syntax, die auf .NET und C# basiert. Die Verb-Nomen-Konvention für Cmdlets (`Get-Item`, `Set-Item`) und die Verwendung von Klammern und Pfeifen (`|`) können für Bash-Nutzer ungewohnt sein.
- **Pipes verhalten sich anders**: In Bash leiten Pipes den rohen Text von einem Befehl zum nächsten weiter. In PowerShell werden stattdessen Objekte übergeben, was zu unerwarteten Ergebnissen führen kann, wenn man Text-basierte Pipelines erwartet.

### 2. Leistungsaspekte

- **Startzeit**: PowerShell kann langsamer starten als Bash, insbesondere auf Systemen mit geringeren Ressourcen.
- **Ressourcennutzung**: PowerShell kann ressourcenintensiver sein als Bash, vor allem wegen seiner .NET-Basis und objektorientierten Natur.

### 3. Plattformübergreifende Unterschiede

- **Nicht alle Funktionen sind plattformübergreifend**: Einige Cmdlets und Funktionen, die in Windows PowerShell verfügbar sind, funktionieren möglicherweise nicht identisch oder sind in PowerShell Core auf Linux/Mac nicht verfügbar.
- **Pfad-Unterschiede**: Der Umgang mit Dateipfaden ist unterschiedlich, z.B. verwendet PowerShell Backslashes `\` wie Windows, während Unix-Systeme Slashes `/` verwenden.

### 4. Komplexität bei der Objektmanipulation

- **Umgang mit Objekten**: PowerShell arbeitet mit .NET-Objekten, was leistungsfähig, aber komplex sein kann. Die Manipulation von Objekten erfordert oft ein tieferes Verständnis von Methoden und Eigenschaften dieser Objekte.

### 5. Scripting Unterschiede

- **Skriptsicherheit**: PowerShell hat ein striktes Ausführungsrichtlinien-System (`Set-ExecutionPolicy`), das standardmäßig das Ausführen von Skripten verhindern kann. Dies kann anfangs verwirrend sein.
- **Fehlerbehandlung**: PowerShell verwendet try-catch-Blöcke für die Fehlerbehandlung, was von der einfachen Fehlerüberprüfung in Bash abweicht.

### Tipps für den Umstieg

1. **Beginnen Sie mit Grundlagen**: Konzentrieren Sie sich zuerst auf grundlegende Cmdlets und den Umgang mit Objekten.
2. **Lernen Sie die PowerShell-Pipeline kennen**: Verstehen Sie, wie Objekte anstelle von Text in Pipelines verwendet werden.
3. **Nutzen Sie die umfangreichen Hilfeoptionen**: `Get-Help` ist ein leistungsstarkes Werkzeug in PowerShell.
4. **Experimentieren Sie in einer sicheren Umgebung**: Nutzen Sie virtuelle Maschinen oder Container, um Ihre Skripte zu testen.
5. **Verwenden Sie geeignete Entwicklungswerkzeuge**: Tools wie Visual Studio Code können mit ihrer PowerShell-Integration sehr hilfreich sein.
6. **Nutzen Sie Community-Ressourcen**: Foren, Dokumentationen und Tutorials können den Übergang erleichtern.

Für Linux-Administratoren bietet PowerShell eine erweiterte, objektorientierte Umgebung, die sich ideal für die Automatisierung komplexer, datenintensiver Aufgaben eignet. Es ermöglicht eine nahtlose Integration in Windows-Umgebungen und bietet durch PowerShell Core und SSH-Unterstützung eine echte plattformübergreifende Kompatibilität. Während die Syntax und der Ansatz sich von Bash oder Zsh unterscheiden, bietet PowerShell ein mächtiges Werkzeugset, das in modernen, heterogenen Netzwerkumgebungen immer wichtiger wird.

Der Wechsel von Bash zu PowerShell erfordert allerdings Anpassungen und ein Umdenken, insbesondere im Hinblick auf objektorientiertes Scripting und die .NET-Integration. Mit Zeit und Übung kann jedoch die mächtige und flexible Natur von PowerShell für komplexe Automatisierungsaufgaben in heterogenen Umgebungen sehr nützlich sein.


## Rezepte

Im Folgenden sind einige Rezepte gesammelt. Mein persönlicher Lernpfad gestaltete sich so, dass ich die Problemstellung als Prompt in ChatGPT formuliert habe und die generierten Erklärungen angepasst und den Code auf meinem dienstlich gelieferten Windows getestet habe.


### Umgebungsvariablen und Pfade

In PowerShell können Sie Umgebungsvariablen auf verschiedene Weisen setzen und verwenden. Umgebungsvariablen sind nützlich, um Informationen systemweit oder pro Sitzung zu speichern und sind überall in Ihrem System verfügbar.

### Umgebungsvariablen setzen

1. **Für die aktuelle Sitzung setzen**: Um eine Umgebungsvariable für die aktuelle PowerShell-Sitzung zu setzen, verwenden Sie:

   ```powershell
   $env:MeineVariable = "Wert"
   ```

   Dieser Wert bleibt bis zum Ende der aktuellen PowerShell-Sitzung erhalten.

2. **Systemweit setzen (permanent)**: Um eine Umgebungsvariable dauerhaft zu setzen, so dass sie auch nach einem Neustart oder in anderen Sitzungen verfügbar ist, verwenden Sie die [System.Environment] Klasse:

   ```powershell
   [System.Environment]::SetEnvironmentVariable("MeineVariable", "Wert", [System.EnvironmentVariableTarget]::Machine)
   ```

   Oder für den aktuellen Benutzer:

   ```powershell
   [System.Environment]::SetEnvironmentVariable("MeineVariable", "Wert", [System.EnvironmentVariableTarget]::User)
   ```

   Für diese Änderungen benötigen Sie möglicherweise Administratorrechte, und sie sind erst nach einem Neustart des Systems oder Neustart der Sitzung sichtbar.

### Umgebungsvariablen verwenden

Um den Wert einer Umgebungsvariablen in einem PowerShell-Skript zu verwenden, greifen Sie einfach auf sie über `$env:Variablenname` zu. Zum Beispiel:

```powershell
$meineVariable = $env:MeineVariable
Write-Host "Der Wert der Umgebungsvariablen ist: $meineVariable"
```

### Wichtige Punkte

- Änderungen, die mit `$env:MeineVariable = "Wert"` gemacht werden, sind nur in der aktuellen PowerShell-Sitzung gültig.
- Änderungen, die mit `[System.Environment]::SetEnvironmentVariable()` gemacht werden, sind dauerhaft, benötigen aber eventuell Administratorrechte und sind erst nach einem Neustart oder einer neuen Sitzung wirksam.
- Umgebungsvariablen sind oft ein Schlüsselbestandteil beim Schreiben von Skripten, insbesondere wenn es um die Konfiguration von Pfaden und anderen systemweiten Einstellungen geht.

Diese Methoden ermöglichen es Ihnen, Umgebungsvariablen flexibel in Ihren PowerShell-Skripten zu nutzen und zu verwalten.

### Pfade

Ja, in PowerShell können Sie den Suchpfad, der in der Umgebungsvariablen `PATH` gespeichert ist, sowohl anzeigen als auch ändern. Der `PATH` ist eine kritische Umgebungsvariable, die angibt, in welchen Verzeichnissen das Betriebssystem nach ausführbaren Dateien sucht.

### Anzeigen des Aktuellen Suchpfads

Um den aktuellen Wert der `PATH`-Umgebungsvariablen anzuzeigen, verwenden Sie:

```powershell
$env:PATH
```

Dieser Befehl gibt die einzelnen Pfade zurück, die durch Semikolons getrennt sind.

### Den Suchpfad Ändern

Um einen neuen Pfad zum `PATH` hinzuzufügen, können Sie den vorhandenen Wert von `$env:PATH` ändern und Ihren neuen Pfad hinzufügen. Zum Beispiel, um `/usr/local/bin` zum `PATH` hinzuzufügen:

```powershell
$env:PATH += ";C:\MeinNeuerPfad"
```

Beachten Sie, dass Änderungen am `PATH` mittels `$env:PATH` nur für die aktuelle Sitzung gelten. Nach dem Schließen der PowerShell-Sitzung gehen diese Änderungen verloren.

### Dauerhafte Änderung des Suchpfads

Um den `PATH` dauerhaft zu ändern, müssen Sie eine systemweite Umgebungsvariable ändern. Das erfordert in der Regel Administratorrechte. Hier ist, wie Sie es tun können:

```powershell
$path = [System.Environment]::GetEnvironmentVariable("PATH", [System.EnvironmentVariableTarget]::Machine)
$newPath = $path + ";C:\MeinNeuerPfad"
[System.Environment]::SetEnvironmentVariable("PATH", $newPath, [System.EnvironmentVariableTarget]::Machine)
```

In diesem Beispiel wird `C:\MeinNeuerPfad` dauerhaft zum System-`PATH` hinzugefügt. Ändern Sie `[System.EnvironmentVariableTarget]::Machine` zu `[System.EnvironmentVariableTarget]::User`, wenn Sie den `PATH` nur für den aktuellen Benutzer ändern möchten.

### Wichtige Hinweise

- Ändern Sie die `PATH`-Umgebungsvariable mit Vorsicht. Falsche Änderungen können dazu führen, dass Betriebssystembefehle und Anwendungen nicht mehr richtig funktionieren.
- Nachdem Sie Änderungen an der `PATH`-Umgebungsvariable vorgenommen haben, müssen Sie möglicherweise Ihren Computer neu starten oder sich ab- und wieder anmelden, damit die Änderungen wirksam werden.
- Wenn Sie den Pfad in einer Unternehmensumgebung oder auf einem Server ändern, stellen Sie sicher, dass Sie die Auswirkungen verstehen und Backup-Pläne haben, falls etwas schief geht.


### Parameter mitgeben

In PowerShell können Sie Parameter in Ihren Skripten oder Funktionen auf verschiedene Arten auswerten und verarbeiten. Hier ist eine grundlegende Anleitung, wie Sie Parameter definieren und verwenden:

1. **Parameter definieren:**

   In einem PowerShell-Skript oder einer Funktion können Sie Parameter mit dem `param`-Block definieren. Zum Beispiel:

   ```powershell
   param (
       [string]$Name,
       [int]$Alter
   )
   ```

   In diesem Beispiel werden zwei Parameter definiert: `$Name` (ein String) und `$Alter` (ein Integer).

2. **Parameter mit Typen und Validierungen:**

   Sie können Typen für Ihre Parameter angeben und verschiedene Validierungen hinzufügen. Zum Beispiel:

   ```powershell
   param (
       [string]$Name,
       [ValidateRange(1,120)]
       [int]$Alter
   )
   ```

   Hier wird sichergestellt, dass der Wert für `$Alter` zwischen 1 und 120 liegt.

3. **Parameter in der Funktion/Skript verwenden:**

   Innerhalb Ihres Skripts oder Ihrer Funktion können Sie diese Parameter wie normale Variablen verwenden:

   ```powershell
   Write-Host "Name: $Name, Alter: $Alter"
   ```

4. **Positionale und benannte Parameter:**

   PowerShell unterstützt sowohl positionale als auch benannte Parameter. Positionale Parameter können in der Reihenfolge, in der sie definiert sind, übergeben werden. Benannte Parameter können in jeder Reihenfolge und explizit durch ihren Namen übergeben werden.

   Beispielaufruf mit positionalen Parametern:

   ```powershell
   .\MeinSkript.ps1 John 30
   ```

   Beispielaufruf mit benannten Parametern:

   ```powershell
   .\MeinSkript.ps1 -Name John -Alter 30
   ```

5. **Optionale Parameter und Standardwerte:**

   Sie können auch optionale Parameter mit Standardwerten definieren:

   ```powershell
   param (
       [string]$Name = "Unbekannt",
       [int]$Alter
   )
   ```

   Hier wird `$Name` den Standardwert "Unbekannt" haben, wenn beim Aufruf kein Wert übergeben wird.

6. **Parameter in einer erweiterten Funktion:**

   Bei der Erstellung erweiterter Funktionen können Sie die `param`-Deklaration innerhalb der Funktion verwenden:

   ```powershell
   function Gruß {
       param (
           [string]$Name,
           [int]$Alter
       )
       Write-Host "Hallo, mein Name ist $Name und ich bin $Alter Jahre alt."
   }
   ```

   Und Sie rufen die Funktion so auf:

   ```powershell
   Gruß -Name "John" -Alter 30
   ```

Durch die Verwendung dieser Techniken können Sie Ihre PowerShell-Skripte und Funktionen flexibel und robust gestalten, wobei Sie die Eingaben kontrollieren und die Benutzerführung verbessern.


### Funktionen

In PowerShell werden Funktionen oft verwendet, um Skripte modular und wiederverwendbar zu gestalten. Eine Funktion in PowerShell kann Parameter annehmen und Werte zurückgeben. Hier ist eine allgemeine Anleitung, wie Sie Funktionen mit Parametern und Rückgabewerten schreiben können:

#### Funktion mit Parametern

Um eine Funktion mit Parametern zu definieren, verwenden Sie den `param`-Block innerhalb der Funktion. Zum Beispiel:

```powershell
function Addiere-Zahlen {
    param (
        [int]$Zahl1,
        [int]$Zahl2
    )

    # Berechnung
    $Summe = $Zahl1 + $Zahl2

    # Rückgabe des Ergebnisses
    return $Summe
}
```

In diesem Beispiel akzeptiert die Funktion `Addiere-Zahlen` zwei Integer-Parameter (`$Zahl1` und `$Zahl2`) und gibt ihre Summe zurück.

#### Aufrufen der Funktion mit Parametern

Sie können die Funktion dann mit Argumenten aufrufen:

```powershell
$ergebnis = Addiere-Zahlen -Zahl1 5 -Zahl2 10
Write-Host "Das Ergebnis ist: $ergebnis"
```

#### Rückgabewerte

PowerShell-Funktionen können Werte auf verschiedene Arten zurückgeben:

1. **Mit dem `return`-Befehl**: Wie im obigen Beispiel gezeigt, können Sie `return` verwenden, um einen Wert explizit zurückzugeben.
2. **Ohne `return`**: PowerShell gibt auch Werte zurück, die nicht explizit mit `return` gekennzeichnet sind. Jeder Ausdruck oder Wert, der nicht einer Variablen zugewiesen ist, wird automatisch zurückgegeben.

#### Beispiel: Funktion mit Rückgabewert ohne `return`

```powershell
function Get-Multiplikation {
    param (
        [int]$Zahl1,
        [int]$Zahl2
    )

    # Das Ergebnis wird automatisch zurückgegeben
    $Zahl1 * $Zahl2
}

$ergebnis = Get-Multiplikation -Zahl1 6 -Zahl2 7
Write-Host "Das Ergebnis ist: $ergebnis"
```

In diesem Beispiel gibt die Funktion `Get-Multiplikation` das Produkt von `$Zahl1` und `$Zahl2` zurück, ohne dass das Schlüsselwort `return` explizit verwendet wird.

#### Wichtige Hinweise

- Funktionen in PowerShell können komplexe Datenstrukturen wie Objekte, Arrays und Hash-Tabellen zurückgeben.
- Die Parameter können auch Typen wie `string`, `bool`, Arrays und benutzerdefinierte Objekte annehmen.
- Sie können Parameter in Funktionen auch optional machen, indem Sie Standardwerte zuweisen.
- PowerShell unterstützt auch fortgeschrittene Funktionen mit Features wie Parameter-Validierung, Parameter-Sets und dynamischen Parametern.

Das Verwenden von Funktionen in PowerShell-Skripten hilft dabei, Ihren Code sauber, organisiert und leicht wiederverwendbar zu halten.


### Aufzählungen

In PowerShell können Sie eine Liste oder ein Array von Elementen, wie Hostnamen, durchlaufen und für jedes Element eine Funktion aufrufen. Hier ist ein Beispiel, wie Sie dies umsetzen können:

1. **Definieren Sie eine Funktion**: Zuerst definieren Sie eine Funktion, die einen Hostnamen als Parameter akzeptiert und die gewünschte Aktion ausführt.

   ```powershell
   function VerarbeiteHostname {
       param (
           [string]$Hostname
       )
       # Führen Sie hier Aktionen mit dem Hostnamen aus
       Write-Host "Verarbeite: $Hostname"
   }
   ```

2. **Erstellen Sie eine Liste von Hostnamen**: Erstellen Sie eine Liste oder ein Array mit den Hostnamen, die Sie verarbeiten möchten.

   ```powershell
   $hostnamen = @("host1.example.com", "host2.example.com", "host3.example.com")
   ```

3. **Durchlaufen Sie die Liste mit einer Schleife**: Verwenden Sie eine Schleife, um die Liste zu durchlaufen und rufen Sie für jeden Hostnamen in der Liste Ihre Funktion auf.

   ```powershell
   foreach ($hostname in $hostnamen) {
       VerarbeiteHostname -Hostname $hostname
   }
   ```

Dieses Beispiel zeigt folgendes:

- Die Funktion `VerarbeiteHostname` wird für jeden Hostnamen in der Liste `$hostnamen` aufgerufen.
- Die `foreach`-Schleife geht durch jedes Element in `$hostnamen` und übergibt es als Argument an die Funktion `VerarbeiteHostname`.

Diese Methode ist sehr effektiv, um eine Aktion für eine Reihe von Elementen in PowerShell durchzuführen. Sie können die Logik in der Funktion `VerarbeiteHostname` anpassen, um die spezifischen Aufgaben zu erfüllen, die Sie für jeden Hostnamen durchführen möchten.


### Kurschlussoperator und Verzweigungen

In PowerShell gibt es keine direkten Äquivalente zu den Bash-Kurzschlussoperatoren `&&` und `||`, die in der Befehlszeilen-Syntax verwendet werden, um Befehle basierend auf dem Erfolg oder Misserfolg des vorherigen Befehls auszuführen. Allerdings gibt es Wege, um ähnliche Funktionalitäten in PowerShell zu erreichen.

### Alternativen in PowerShell

1. **Verwendung von `if`-Anweisungen**:
   - Sie können `if`-Anweisungen verwenden, um ähnliche Logik wie `&&` und `||` zu erreichen. Zum Beispiel:

     ```powershell
     $command1 = Test-Path "C:\BeispielDatei.txt"
     if ($command1) { Write-Host "Datei existiert" }
     ```

     In diesem Beispiel wird `Write-Host "Datei existiert"` nur ausgeführt, wenn `Test-Path "C:\BeispielDatei.txt"` wahr (`$true`) zurückgibt, was dem `&&`-Operator in Bash entspricht.

2. **Verwendung des `-and` und `-or` Operators**:
   - PowerShell bietet die logischen Operatoren `-and` und `-or`, die innerhalb von Ausdrücken verwendet werden können:

     ```powershell
     ($command1 -and $command2)
     ```

     Hierbei werden `$command1` und `$command2` als boolesche Ausdrücke ausgewertet, was nicht ganz dasselbe ist wie die sequenzielle Ausführung von Befehlen mit `&&` und `||` in Bash.

3. **Try-Catch-Blöcke für Fehlerbehandlung**:
   - Für eine fortgeschrittene Fehlerbehandlung können Sie `try-catch`-Blöcke verwenden. Dies ist besonders nützlich, wenn Sie Fehler von Befehlen abfangen und darauf reagieren möchten.

### Beispiel für `if`-Anweisung:

Ein praktisches Beispiel für eine `if`-Anweisung, die einem Kurzschlussoperator ähnelt:

```powershell
if (Test-Connection google.com -Quiet) { 
    Write-Host "Internetverbindung ist aktiv" 
} else { 
    Write-Host "Keine Internetverbindung" 
}
```

In diesem Beispiel wird der zweite Befehl (`Write-Host "Internetverbindung ist aktiv"`) nur ausgeführt, wenn der erste Befehl (`Test-Connection`) erfolgreich ist.

Zusammenfassend: Während PowerShell nicht direkt die `&&` und `||` Operatoren wie in Unix-Shells unterstützt, können Sie ähnliche Logiken mit `if`-Anweisungen, logischen Operatoren und Fehlerbehandlungsstrukturen implementieren.


## VMware PowerCLI

VMware PowerCLI ist eine Befehlszeilenschnittstelle und ein Skripting-Tool, das auf Windows basiert und für die Verwaltung und Automatisierung von VMware vSphere-Umgebungen entwickelt wurde. Es basiert auf Microsoft PowerShell und war ursprünglich nur für Windows verfügbar.

Für Linux- und macOS-Benutzer gibt es jedoch Möglichkeiten, PowerCLI zu nutzen:

1. **PowerShell Core nutzen:** Mit der Einführung von PowerShell Core, einer plattformübergreifenden Version von PowerShell, können Linux- und macOS-Benutzer nun PowerShell-Skripte ausführen. VMware hat PowerCLI mit PowerShell Core kompatibel gemacht, sodass Sie es auf Nicht-Windows-Systemen verwenden können.

   Um PowerCLI unter Linux zu verwenden, führen Sie die folgenden Schritte aus:

   - **PowerShell Core installieren:** Zuerst müssen Sie PowerShell Core auf Ihrem Linux-System installieren. Besuchen Sie die [offizielle PowerShell GitHub-Seite](https://github.com/PowerShell/PowerShell) für Installationsanweisungen.

   - **VMware PowerCLI installieren:** Nachdem PowerShell Core installiert ist, können Sie PowerCLI installieren. Starten Sie PowerShell und führen Sie den folgenden Befehl aus:

     ```powershell
     Install-Module -Name VMware.PowerCLI -Scope CurrentUser
     ```

   - **Konfiguration:** Es könnte notwendig sein, die Ausführungsrichtlinie zu ändern, um Skripte auszuführen. Dies können Sie mit dem Befehl `Set-ExecutionPolicy` tun.

2. **Docker-Container:** Eine weitere Möglichkeit ist die Verwendung von Docker. Es gibt Docker-Images, die PowerShell und PowerCLI enthalten, sodass Sie PowerCLI in einem Container auf jedem System ausführen können, das Docker unterstützt, einschließlich Linux.

3. **Virtuelle Maschine oder Dual-Boot:** Eine weniger elegante, aber praktikable Lösung wäre die Nutzung einer Windows-basierten virtuellen Maschine auf Ihrem Linux-System, um PowerCLI auszuführen, oder das Einrichten eines Dual-Boot-Systems mit Windows.

Die Wahl der Methode hängt von Ihren spezifischen Anforderungen und Ihrer Systemumgebung ab. Die Verwendung von PowerShell Core ist wahrscheinlich der direkteste Ansatz, um VMware PowerCLI auf einem Linux-System zu nutzen.


Gerne, ich kann Ihnen einige grundlegende PowerCLI-Befehle und Skripte für häufige Aufgaben wie das Einloggen in einen vCenter Server, das Erstellen einer VM aus einer Vorlage, das Erstellen und Löschen von Snapshots usw. bereitstellen. 

Bitte beachten Sie, dass Sie vor der Ausführung dieser Skripte sicherstellen sollten, dass PowerCLI ordnungsgemäß installiert ist und dass Sie die notwendigen Berechtigungen haben, um diese Aktionen in Ihrer vSphere-Umgebung durchzuführen.

1. **Login in vCenter oder ESXi Host:**

   ```powershell
   Connect-VIServer -Server <Server-Name oder IP> -User <Benutzername> -Password <Passwort>
   ```

2. **VM aus einer Vorlage erstellen:**

Das folgende einfache Skript, erstellt eine VM aus einer Vorlage. Sie müssen die Variablen wie Namen der Vorlage, VM-Name, Datastore, Cluster etc. entsprechend Ihrer Umgebung anpassen.

```powershell!
        $vmName = "myNewVm01"
        $vmHost = Get-VMHost -Name "vsphere.example.net"
        $dataStore = Get-Datastore -Name "testvc-190"
        $template = Get-Template -Name "debian-12-bare"
        $location = Get-Folder -Name "netlab"
        $resourcePool = Get-ResourcePool -Name "netlab"

        New-VM -Name $vmName -Template $template -VMHost $vmHost -Datastore $dataStore -Location $location -ResourcePool $resourcePool
        Start-VM -VM $vmName
        $ipv4Address = (Get-VM -Name $vmName).Guest.IPAddress[0]
```




3. **Snapshot einer VM erstellen:**

Vor Konfigurationsänderungen an VMs bietet vSphere die Möglichkeit, einen Snapshot zu machen:

   ```powershell
   $vm = Get-VM -Name "NameDerVM"
   New-Snapshot -VM $vm -Name "SnapshotName" -Description "Beschreibung des Snapshots"
   ```

4. **Alle Snapshots einer VM löschen:**

   ```powershell
   $vm = Get-VM -Name "NameDerVM"
   Get-Snapshot -VM $vm | Remove-Snapshot -Confirm:$false
   ```

5. **VM löschen:**

Am Ende des Entwicklungszyklus möchte man die Maschine wieder loswerden:

   ```powershell
   Remove-VM -VM "NameDerVM" -DeletePermanently -Confirm:$false
   ```

## Fortgeschrittene Konzepte

### Remote-Ausführung

Oft möchte man ein Kommando nicht auf der loaklen Maschine ausführen, sondern zum Beispiel auf einem Windows Server. In PowerShell können Sie eine Remote-Verbindung zu einem  Server herstellen und Cmdlets dort ausführen, vorausgesetzt, Sie haben die entsprechenden Berechtigungen und die Remote-Verwaltung ist konfiguriert.

In dem folgenden Beispiel wollen wir das `Add-DnsServerResourceRecordA` Cmdlet von einer Windows-Workstation aus auf einem Remote-DNS-Server auszuführen:

### 1. Aktivieren der PowerShell-Remoting-Funktion

Zunächste stellen wir sicher, dass PowerShell Remoting auf dem Ziel-DNS-Server aktiviert ist. Dies kann in der Regel durch Ausführen des folgenden Cmdlets auf dem DNS-Server erfolgen:

```powershell
Enable-PSRemoting -Force
```

### 2. Verwenden von PowerShell-Remoting

Von Ihrer Workstation aus können Sie eine Remote-PowerShell-Sitzung mit dem DNS-Server starten. Verwenden Sie dazu das `Enter-PSSession` oder `Invoke-Command` Cmdlet. Zum Beispiel:

```powershell
$dnsServer = "NameOderIPDesDNSServers"
$credential = Get-Credential

Invoke-Command -ComputerName $dnsServer -Credential $credential -ScriptBlock {
    Add-DnsServerResourceRecordA -Name "RecordName" -ZoneName "IhreZone.com" -IPv4Address "IP-Adresse" -TimeToLive 3600
}
```


- `$dnsServer` sollte durch den Namen oder die IP-Adresse Ihres DNS-Servers ersetzt werden.
- `$credential` holt Ihre Anmeldeinformationen, die für die Authentifizierung am DNS-Server verwendet werden.
- Der `Invoke-Command`-Befehl führt das `Add-DnsServerResourceRecordA` Cmdlet auf dem DNS-Server aus.

### 3. Beachten Sie die Sicherheitsaspekte

- Stellen Sie sicher, dass die notwendigen Firewall-Regeln konfiguriert sind, um PowerShell Remoting zu erlauben.
- Verwenden Sie sichere Anmeldeinformationen und beschränken Sie den Zugriff auf vertrauenswürdige Benutzer.

Die Verwendung von Remote-PowerShell-Cmdlets ist eine leistungsstarke Methode, um Aufgaben auf Servern aus der Ferne zu automatisieren, ohne sich direkt auf diesen Servern anmelden zu müssen.

